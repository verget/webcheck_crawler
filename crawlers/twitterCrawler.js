var config = require('../config.js');
var Twitter = require('twitter');
var TwitterModel = require('../models/twitterModel.js');

var _this = this;

var client = new Twitter(config.twitter.credentials);

//TODO edit to make more info for model
_this.getInfo = function(username, cb) {
    console.log('Request user - ' + username + '  - on twitter');
    client.get('users/show', {screen_name: username}, function(err, data){
        if(err) {
            cb(err);
        }
        var twitterModel = new TwitterModel();
        twitterModel.name = data.name;
        twitterModel.screen_name = data.screen_name;
        twitterModel.description = data.description;
        twitterModel.location = data.location;
        twitterModel.timezone = data.time_zone;
        twitterModel.total_tweets = data.statuses_count;
        twitterModel.following = data.friends_count;
        twitterModel.followers = data.followers_count;
        twitterModel.registered_at = data.created_at;
        twitterModel.favourite_posts = data.favourites_count;
        twitterModel.last_tweet = data.status.text;
        console.log(twitterModel);

        twitterModel.save(function(err) {
            if (err) {
                return cb(err, null);
            }
            cb(null, twitterModel);
        });
    });
};

module.exports = _this;