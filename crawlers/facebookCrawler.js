var config = require('../config.js');

var fbGraph = require('fbgraph');
var FacebookModel = require('../models/facebookModel.js');

fbGraph.setAccessToken(config.facebook.credentials.token);

var _this = this;

_this.getInfo = function(username, cb) {

    var fields = [
        'id',
        'about',
        //'access_token',
        //'ad_campaign',
        'affiliation',
        'app_id',
        'app_links',
        'artists_we_like',
        'attire',
        'awards',
        'band_interests',
        'band_members',
        'best_page',
        'bio',
        'birthday',
        'booking_agent',
        'built',
        'business',
        'can_checkin',
        'can_post',
        'category',
        'category_list',
        'checkins',
        'company_overview',
        'contact_address',
        //'context',
        'country_page_likes',
        'cover',
        'culinary_team',
        'current_location',
        'description',
        'description_html',
        'directed_by',
        'display_subtext',
        'emails',
        //'engagement',
        //'fan_count',
        'featured_video',
        'features',
        'food_styles',
        'founded',
        'general_info',
        'general_manager',
        'genre',
        'global_brand_page_name',
        'global_brand_root_id',
        'has_added_app',
        'hometown',
        'hours',
        'impressum',
        'influences',
        'is_always_open',
        'is_community_page',
        'is_permanently_closed',
        'is_published',
        'is_unclaimed',
        'is_verified',
        'last_used_time',
        'leadgen_tos_accepted',
        'link',
        'location',
        'members',
        'mission',
        'mpg',
        'name',
        'name_with_location_descriptor',
        'network',
        'new_like_count',
        'offer_eligible',
        //'owner_business',
        'parent_page',
        'parking',
        'payment_options',
        'personal_info',
        'personal_interests',
        'pharma_safety_info',
        'phone',
        'place_type',
        'plot_outline',
        'press_contact',
        'price_range',
        'produced_by',
        'products',
        //'promotion_eligible',
        //'promotion_ineligible_reason',
        'public_transit',
        'record_label',
        'release_date',
        'restaurant_services',
        'restaurant_specialties',
        'schedule',
        'screenplay_by',
        'season',
        'single_line_address',
        'starring',
        'start_info',
        'store_location_descriptor',
        'store_number',
        'studio',
        //'supports_instant_articles',
        'talking_about_count',
        //'unread_message_count',
        //'unread_notif_count',
        //'unseen_message_count',
        'username',
        'verification_status',
        'voip_info',
        'website',
        'were_here_count',
        'written_by'
    ];

    var fieldsString = fields.join(',');

    //TODO add error that you cannot access a person's profile --> only groups/pages etc
    fbGraph.get(username + '?fields=' + fieldsString, function(err, data) {
        console.log(username);
        if (err) {
            console.log(err);
            cb(err);
        }
        console.log(data);

        var fbModel = new FacebookModel();
        fbModel.id = data.id;
        fbModel.about = data.about;
        fbModel.affiliation = data.affiliation;
        fbModel.app_id = data.app_id;
        fbModel.app_links = data.app_links;
        fbModel.artists_we_like = data.id;
        fbModel.attire = data.about;
        fbModel.awards = data.affiliation;
        fbModel.band_interests = data.app_id;
        fbModel.band_members = data.app_links;
        fbModel.best_page = data.best_page;
        fbModel.bio = data.bio;
        fbModel.birthday = data.birthday;
        fbModel.booking_agent = data.booking_agent;
        fbModel.built = data.built;
        fbModel.business = data.business;
        fbModel.can_checkin = data.can_checkin;
        fbModel.can_post = data.can_post;
        fbModel.category = data.category;
        fbModel.category_list = data.category_list;
        fbModel.checkins = data.checkins;
        fbModel.company_overview = data.company_overview;
        fbModel.contact_address = data.contact_address;
        fbModel.country_page_likes = data.country_page_likes;
        fbModel.cover = data.cover;
        fbModel.culinary_team = data.culinary_team;
        fbModel.current_location = data.current_location;
        fbModel.description = data.description;
        fbModel.description_html = data.description_html;
        fbModel.directed_by = data.directed_by;
        fbModel.best_page = data.best_page;
        fbModel.display_subtext = data.display_subtext;
        fbModel.emails = data.emails;
        fbModel.featured_video = data.featured_video;
        fbModel.features = data.features;
        fbModel.food_styles = data.food_styles;
        fbModel.founded = data.founded;
        fbModel.general_info = data.general_info;
        fbModel.general_manager = data.general_manager;
        fbModel.genre = data.genre;
        fbModel.global_brand_page_name = data.id;
        fbModel.global_brand_root_id = data.about;
        fbModel.has_added_app = data.affiliation;
        fbModel.hometown = data.app_id;
        fbModel.hours = data.app_links;
        fbModel.impressum = data.id;
        fbModel.influences = data.about;
        fbModel.is_always_open = data.affiliation;
        fbModel.is_community_page = data.app_id;
        fbModel.is_permanently_closed = data.app_links;
        fbModel.is_published = data.best_page;
        fbModel.is_unclaimed = data.bio;
        fbModel.is_verified = data.birthday;
        fbModel.last_used_time = data.booking_agent;
        fbModel.leadgen_tos_accepted = data.built;
        fbModel.link = data.business;
        fbModel.location = data.can_checkin;
        fbModel.members = data.can_post;
        fbModel.mission = data.category;
        fbModel.mpg = data.category_list;
        fbModel.name = data.name;
        fbModel.name_with_location_descriptor = data.name_with_location_descriptor;
        fbModel.network = data.network;
        fbModel.new_like_count = data.new_like_count;
        fbModel.offer_eligible = data.offer_eligible;
        fbModel.parent_page = data.parent_page;
        fbModel.parking = data.parking;
        fbModel.payment_options = data.payment_options;
        fbModel.personal_info = data.personal_info;
        fbModel.personal_interests = data.personal_interests;
        fbModel.pharma_safety_info = data.pharma_safety_info;
        fbModel.phone = data.phone;
        fbModel.place_type = data.place_type;
        fbModel.plot_outline = data.plot_outline;
        fbModel.press_contact = data.press_contact;
        fbModel.produced_by = data.produced_by;
        fbModel.products = data.products;
        fbModel.public_transit = data.public_transit;
        fbModel.record_label = data.record_label;
        fbModel.release_date = data.release_date;
        fbModel.restaurant_services = data.restaurant_services;
        fbModel.restaurant_specialties = data.restaurant_specialties;
        fbModel.schedule = data.schedule;
        fbModel.screenplay_by = data.screenplay_by;
        fbModel.season = data.season;
        fbModel.single_line_address = data.single_line_address;
        fbModel.starring = data.starring;
        fbModel.start_info = data.start_info;
        fbModel.store_location_descriptor = data.store_location_descriptor;
        fbModel.store_number = data.store_number;
        fbModel.studio = data.studio;
        fbModel.talking_about_count = data.talking_about_count;
        fbModel.username = data.username;
        fbModel.verification_status = data.verification_status;
        fbModel.voip_info = data.voip_info;
        fbModel.website = data.website;
        fbModel.were_here_count = data.were_here_count;
        fbModel.written_by = data.written_by;

        fbModel.save(function (err) {
            if (err) {
                cb(err, null);
            }
            cb(null, fbModel);
        });
    });
};

module.exports = _this;