var cheerio = require('cheerio');
var _ = require('underscore');
var urlUtil = require('url');
var async = require('async');

var siteMapModel = require('./../models/siteMapModel');
var mainCrawler = require('./mainCrawler');
var socialDomains = require('./../modules/socialDomains');

var _this = this;

// Edrena recursia
_this.buildSitemap = function (website, deep, callback) {
    var checkedUrls = {};
    var siteMap = new siteMapModel();
    siteMap.websiteId = website._id;
    siteMap.scanStart = new Date();
    siteMap.siteMap = [];

    function deeper(link, parent, urlDone, parentDeep) {
        if (parentDeep >= deep) { // Too deeper, stop it please!!!
            return urlDone(null);
        }
        var myDeep = parentDeep + 1;
        var url = mainCrawler.buildUrl(website.url, (link) ? link : '');
        if (checkedUrls[url]) { // Already checked
            checkedUrls[url].parents.push(parent);
            return urlDone(null);
        }
        checkedUrls[url] = {
            parents: [parent]
        };
        console.log('Request page:', url, 'parent', parent, 'myDeep', myDeep);
        mainCrawler.getPage(url, {saveDb: false, useDom: true}, function (err, dom, res) {
            var result = {
                url: urlUtil.parse(url),
                link: link,
                links: {
                    all: [],
                    local: [],
                    social: [],
                    nonSocial: [],
                    external: [],
                    hashes: []
                },
                parent: [parent],
                loadTime: (res) ? res.loadTime : 0,
                statusCode: 0,
                isLive: _this.isLive(err, res),
                isChecked: true,
                isBinary: false,
                checkedDate: new Date()
            };

            if (err) {
                if (err.code) {
                    switch (err.code) {
                        case 'ENOTFOUND':
                            result.statusCode = 404;
                            break;
                        case 'ECONNRESET':
                            result.statusCode = 404;
                            break;
                    }
                    result.isLive = false;
                } else {
                    console.log(err);
                }
            } else if (res && !res.isBinary) {
                result.statusCode = res.statusCode;
                result.title = res.title;
                result.meta = res.meta;
                result.textHeaders = res.textHeaders;
                result.responseHeaders = res.headers;
            } else if (res && res.isBinary) {
                result.statusCode = res.statusCode;
            }
            siteMapModel.findByIdAndUpdate(siteMap._id, {$push: {siteMap: result}}, function (err, doc) {
                if (err) {
                    console.error(err);
                }
            });

            if (!result.isLive) {
                return urlDone(null);
            }

            if (res.isBinary) {
                return urlDone(null);
            }

            _this.getPageLinks(result.url, dom, function (err, links) {
                if (err) {
                    return urlDone(err);
                }

                checkedUrls[url].links = links;

                var queue = [];
                _.each(links.local, function (newLink) {
                    queue.push(function (doneMe) {
                        deeper(newLink.href, result.url.href, doneMe, myDeep);
                    });
                });
                async.series(queue, urlDone);
            });
        });
    }

    siteMap.save(function () { // Let's begin
        deeper(website.url.href, website.url.href, function (err) { // All done
            siteMapModel.findById(siteMap._id, function(mongoErr, newSiteMap) {
                newSiteMap.siteMap.forEach(function (link, i) {
                    if( checkedUrls[link.link] ) {
                        link.parent = checkedUrls[link.link].parents;
                        _.each(checkedUrls[link.link].links, function(cat, key) {
                            _.each(checkedUrls[link.link].links[key], function(elink) {
                                link.links[key].push(elink);
                            });
                        });
                    }
                });
                newSiteMap.save(function (err) {
                    callback && callback(newSiteMap._id);
                });
            })
        }, 0);
    });
};

_this.getPageLinks = function (parsedUrl, dom, callback) {
    if (!dom || typeof dom != 'function') {
        console.log("Can't get links");
        return callback({code: 5555}); // FIXME need normal error code
    }
    var allLinks = {};
    dom('a').each(function () {
        var href = dom(this).attr('href');
        if (!href || href == '') {
            return;
        }
        href = _this.fixHref(parsedUrl, dom(this).attr('href'));
        allLinks[href] = dom(this);
    });

    var result = {
        all: {},
        local: {},
        social: {},
        nonSocial: {},
        external: {},
        hashes: {}
    };
    _.each(allLinks, function (linkDom, href) { // TODO change to parallel for speed up
        var link = {
            href: href,
            title: dom(linkDom).attr('title'),
            download: dom(linkDom).attr('download'),
            name: dom(linkDom).attr('name'),
            rel: dom(linkDom).attr('rel'),
            rev: dom(linkDom).attr('rev'),
            target: dom(linkDom).attr('target'),
            type: dom(linkDom).attr('type'),
            repeat: 0,
            props: {}
        };

        link.props['isSocial'] = _this.isSocial(href);
        link.props['isExternal'] = _this.isExternal(parsedUrl, href);
        link.props['isHash'] = (href.indexOf('#') === 0);

        if (result.all[href]) {
            result.all[href].repeat++;
        } else {
            result.all[href] = link;
        }

        if (link.props['isSocial']) {
            result.social[href] = link;
            result.external[href] = link;
        } else if (link.props['isExternal']) {
            result.nonSocial[href] = link;
            result.external[href] = link;
        } else if (link.props['isHash']) {
            result.hashes[href] = link;
        } else {
            result.local[href] = link;
        }
    });

    return callback(null, result);
};

_this.isExternal = function (urlObject, href) {
    var hrefObject = urlUtil.parse(href);
    var hrefDomains = [hrefObject.hostname];
    var baseDomains = [urlObject.hostname];

    if (!hrefObject || !hrefObject.hostname) {
        return false;
    }

    if (hrefObject.hostname.indexOf('.') != -1) {
        hrefDomains = hrefObject.hostname.split('.');
        hrefDomains = hrefDomains.reverse();
    }

    if (urlObject.hostname.indexOf('.') != -1) {
        baseDomains = urlObject.hostname.split('.');
        baseDomains = baseDomains.reverse();
    }

    var same = [];
    hrefDomains.forEach(function (domain, index) {
        same[index] = (domain == baseDomains[index]);
    });

    if (same.length > 1) { // if domain.com == domain.com
        return !(same[0] && same[1]);
    }
    return !same[0]; // if localhost == localhost or com == com
};

_this.isSocial = function (href) {
    var result = false;
    socialDomains.forEach(function (domain) {
        if (href.indexOf(domain) != -1) {
            result = true;
        }
    });
    return result;
};

_this.isLive = function (err, res) {
    if (res && res.statusCode) {
        if (res.statusCode >= 100 && res.statusCode < 400) {
            return true;
        }
    }
    return false;
};

_this.fixHref = function (baseUrl, newHref) {

    newHref = newHref.replace(/\s+/g, '');
    if (newHref.indexOf('//') === 0) {
        newHref = baseUrl.protocol + newHref;
    }
    //if (encodeURI(decodeURI(newHref)).toUpperCase() != newHref.toUpperCase()) //check for already encoded
    //    newHref = encodeURI(newHref);

    return encodeURI(decodeURI(newHref));
};

module.exports = _this;