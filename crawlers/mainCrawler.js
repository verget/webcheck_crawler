var _ = require('underscore');
var urlUtil = require('url');
var request = require('request');
var cheerio = require('cheerio');
var config = require('../config.js');
var errors = require('./../modules/errors.js');
var Website = require('../models/websiteModel.js');

var _this = this;

_this.isUrl = function(urlString) {
    var url = urlUtil.parse(urlString);
    if( !url ) {
        return false;
    }
    if( url.protocol && url.hostname ) {
        return url;
    }
    return false;
};

/**
 * Get one page by provided url and parse some data
 * options {
 *  saveDb: Bool - Save result to DB or not
 *  findBeforeRequest: Bool - Find object by href before request (used for update)
 *  useDom: Bool - If true, result html returned as cheerio object otherwise as string html
 * }
 * callback(err, html, siteRes) - without saveDb option (raw html placed to siteRes.html)
 * callback(err, websiteObject, siteRes) - for saveDb option
 * [siteRes] have additional fields
 *  siteRes.meta - contains all meta data meta[name => val] and meta.props[property => val]
 *  siteRes.title - contains page title
 *  siteRes.loadTime - contains request process time in milliseconds
 */
_this.getPage = function(url, options, callback) {
    if (!/^(?:f|ht)tps?:\/\//.test(url)) {
        url = "http://" + url;
    }
    
    var urlObject = urlUtil.parse(url, true);
    if( !urlObject ) {
        return callback(errors.url.parse);
    }
    
    var _options = {
        url: url,
        headers: {
            'User-Agent': 'Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:45.0) Gecko/20100101 Firefox/45.0',
            'Accept': 'text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8',
            'Accept-Encoding': 'gzip,deflate',
            'Accept-Language' : 'en-US,en;q=0.5'
        },
        gzip: true,
        timeout: config.crawler.timeout,
        followAllRedirects: config.crawler.followAllRedirects
    };

    var saveDb = config.crawler.saveDb; //true
    var findBeforeRequest = config.crawler.findBeforeRequest; //false
    
    if( typeof options != 'function' ) { // 2 params mode
        if( options.findBeforeRequest ) { // Update existing website object
            findBeforeRequest = true;
            delete options.findAndUpdate;
        }
        if( options.saveDb == false) { // Don't save website object, just return data
            saveDb = false;
            delete options.saveDb;
        }
        _options = _.extend(_options, options); // override or extend existing options
    } else {
        callback = options;
    }
    
    if( !saveDb ) { // Just get page and return
        return _this.makeRequest(null, _options, callback);
    }
    
    if( findBeforeRequest ) {
        Website.findOne({'url.href': urlObject.href}, function(err, doc){
            if( err ) {
                return console.error(err);
            }
            if( !doc ) {
                var website = new Website();
                website.url = _.extend({}, urlObject);
                website.attributes = {};
                website.save(function(err) {
                    _this.makeRequest(website, _options, callback);
                });
                return;
            }
            _this.makeRequest(doc, _options, callback);
        });
        return;
    }
    // If we no need to find by href, just create new record
    var website = new Website();
    website.url = urlObject;
    website.attributes = {};
    website.save(function(err) {
        _this.makeRequest(website, _options, callback);
    });
};
    
_this.makeRequest = function(website, options, callback) {
    var useDom = config.crawler.useDom;
    if( options.useDom ) { // Use DOM parser
        useDom = true;
        delete options.useDom;
    }
    var startTime = Date.now();
    setTimeout(function(){
        request(options, function(err, siteRes, html){
            if( err || !siteRes ) {
                console.log(err);
                return callback(err, website, null);
            }
            if (siteRes.res)
                console.log(siteRes.headers);
            siteRes.loadTime = Date.now() - startTime;
            siteRes.isBinary = false;
            if (siteRes.headers['content-type'].indexOf('text') == -1){
                siteRes.isBinary = true;
            }
            if( website == null ) { // Just return HTML
                if( !siteRes.isBinary && useDom ) { // DOM mode return HTML as DOM object
                    try {
                        var $ = cheerio.load(html);
                        siteRes.html = html;
                        html = $;
                        siteRes.title = _this.parseTitle(html);
                        siteRes.meta = _this.parseMeta(html);
                        siteRes.textHeaders = _this.parseTextHeaders(html);

                        return callback && callback(null, html, siteRes);
                    }catch(e){
                        return callback(e);
                    }
                }else {
                    return callback && callback(null, html, siteRes);
                }
            }
            if (siteRes.isBinary){
                return callback && callback(null, html, siteRes);
            }
            // Save to DB
            website.attributes.html = html;
            website.attributes.responseHeaders = siteRes.headers;

            website.lastCheck = new Date();

            if( useDom ) { // DOM mode return HTML as DOM object
                var $ = cheerio.load(html);
                siteRes.html = html;
                html = $;
            }
            siteRes.title = _this.parseTitle(html);
            siteRes.meta = _this.parseMeta(html);
            siteRes.textHeaders = _this.parseTextHeaders(html);

            //website.attributes.title = _this.parseTitle(html);
            website.attributes.meta = _this.parseMeta(html);
            website.attributes.textHeaders = _this.parseTextHeaders(html);

            website.save(function(err) { // Save result to DB
                if(err){
                    return callback(err, website, siteRes);
                }
                return callback(null, website, siteRes);
            });
        });
    }, config.crawler.interval);
};

_this.buildUrl = function(baseUrl, href) {
    return urlUtil.resolve(baseUrl.href, href);
};

_this.parseTitle = function(html) {
    if( typeof html != 'string' ) {
        return html('title').text();
    }
    html = html.replace(/\n\r/m, '').replace(/\n/m, '');
    var res = /<title.*?>(.*?)<\/title>/im.exec(html);
    return (res) ? res[1] : '';
};

_this.parseMeta = function(html) {
    if( typeof html != 'string' ) {
        return _this._domMeta(html);
    }
    return _this._nativeMeta(html);
};

_this.parseTextHeaders = function(html) {
    if( typeof html != 'string' ) {
        return _this._domTextHeaders(html);
    }else {
        return _this._nativeTextHeaders(html);
    }
};

_this._domTextHeaders = function(domHtml) {
    var result = {};
    var headers = domHtml(":header");
    _.each(headers, function(val, key){
        var str = '';
        _.each(val.children, function(v, k){
            if (v.data){
                str+= v.data;
            }else{
                if (v.children[0]){
                    str+= v.children[0].data;
                }
            }
        });
        if (!result[val.name]){
            result[val.name] = [];
        }
        result[val.name].push(str);
    });
    return result;
};

_this._nativeTextHeaders = function(html) {

    html = html.replace(/\n\r/mg, ' ').replace(/\n/mg, ' ');

    var regex = /<h(\d)[\s\S]*?>(.*?)<\/h\d>/ig;
    var res;
    var result = {};
    while ((res = regex.exec(html)) !== null) {
        if (!result['h'+res[1]])
            result['h'+res[1]] = [];
        result['h'+res[1]].push(res[2]);
    }
    return result;
};

_this._domMeta = function(domHtml) {
    var metas = domHtml("meta");
    var result = [];
    metas.each(function(key, meta) {
        result.push(meta.attribs);
    });
    return result;
};

_this._nativeMeta = function(html) {

    html = html.replace(/\n\r/mg, ' ').replace(/\n/mg, ' ');

    var regex = /<meta.*?>/ig;
    var regex2 = /\s['"]*?(.*?)['"]*?\s*?=\s*?['"](.*?)['"]/ig;
    var res, res2;
    var result = [];
    while ((res = regex.exec(html)) !== null) {
        var obj = {}
        while ((res2 = regex2.exec(res[0])) !== null) {
            delete res2.input;
            obj[res2[1]] = res2[2];
        }
        result.push(obj);
    }

    return result;

};
module.exports = this;
