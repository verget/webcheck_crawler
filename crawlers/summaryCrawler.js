var psi = require('psi');
_this = this;

_this.getSummaryInfo = function(website, callback){

    var startTime = Date.now();
    psi(website.url.href, {nokey: 'true', strategy: 'mobile'}).then(function (data) { //todo user select
        website.id = data.id;
        website.title = data.title;
        website.responseCode = data.responseCode;
        website.pageStats = data.pageStats;
        website.version = data.version;
        website.ruleGroups = data.ruleGroups;
        website.loadTime = Date.now() - startTime;
        website.save(function(err) {
            if (err) {
                return callback(err);
            }
            callback(null);
        });
    });
};

module.exports = _this;