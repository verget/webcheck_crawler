module.exports = {
    url: {
        code: 2000,
        parse: {
            code: 1101,
            key: 'Url parse error',
            desc: 'Wrong url provided'
        }
    },
};