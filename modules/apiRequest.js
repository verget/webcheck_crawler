
var request = require('request');
var config = require('../config.js');
var async = require('async');
var urlUtil = require('url');

var mainCrawler = require('../crawlers/mainCrawler');
var linksCrawler = require('../crawlers/linksCrawler');
var summaryCrawler = require('../crawlers/summaryCrawler');
var twitterCrawler = require('../crawlers/twitterCrawler');
var facebookCrawler = require('../crawlers/facebookCrawler');
var linkedinCrawler = require('../crawlers/linkedinCrawler');

var ApiRequest = require('../models/apiRequestModel');


var _this = this;

_this.setRequest = function(requestId, callback){
    callback(null, 'Request '+requestId+' received');
    ApiRequest.findById(requestId, function(err, request){
        if (err || !request){
            console.log(err);
        }else{
            if (request.status == 'sent'){
                request.status = 'recieved';
                request.save(function(err){
                    _this.makeTests(request);
                });
            }
        }
    });
};


_this.makeTests = function(request){
    var functions = [];
    request.tests.forEach(function (obj) {
        switch (obj.type) {
            case 'siteMap': functions.push(function(callback){
                _this.makeSiteMapTest(request, callback)
            });
                break;
            case 'info': functions.push(function(callback){
                _this.makeInfoTest(request, callback)
            });
                break;
            case 'facebook': functions.push(function(callback){
                _this.makeFacebookTest(request, callback)
            });
                break;
            case 'twitter': functions.push(function(callback){
                _this.makeTwitterTest(request, callback)
            });
                break;
            case 'linkedin': functions.push(function(callback){
                _this.makeLinkedInTest(request, callback)
            });
                break;
        }
    });
    request.tests = [];
    request.save();
    async.parallel(functions, function(){
        console.info('All test done');
        request.status = 'completed';
        request.save(function(err){
            if (err){
                _this.sendAnswer(request._id, err);
            }else{
                _this.sendAnswer(request._id, null);
            }
        });
    });

};

_this.makeSiteMapTest = function(request, callback){

    mainCrawler.getPage(request.url, {findBeforeRequest: true}, function(err, website) {
        if( err ) {
            if( err.code ) {
                switch (err.code) {
                    case 'ENOTFOUND': callback(err); break;
                    case 'ECONNRESET': callback(err); break;
                    default : callback(err); break;
                }
            }
            return;
        }
        if( !website ) {
            website = new Website();
            website.url = request.url;
            website.save(function() {
                summaryCrawler.getSummaryInfo(website, function(err){
                    if (!err){
                        linksCrawler.buildSitemap(website, config.crawler.deep, function(siteMapId){
                            console.info('Build map done');
                            _this.saveToRequest(request._id, siteMapId, 'siteMap', callback);
                        });
                    }
                });
            });
            return;
        }
        summaryCrawler.getSummaryInfo(website, function(err){
            if (!err){
                linksCrawler.buildSitemap(website, config.crawler.deep, function(siteMapId){
                    console.info('Build map done');
                    _this.saveToRequest(request._id, siteMapId, 'siteMap', callback);
                });
            }
        });
    });
};

_this.makeInfoTest = function(request, callback){
    mainCrawler.getPage(request.url, {findBeforeRequest: true}, function(err, website) {
        if( err ) {
            if( err.code ) {
                switch (err.code) {
                    case 'ENOTFOUND': callback(err); break;
                    case 'ECONNRESET': callback(err); break;
                    default : callback(err); break;
                }
            }
            return;
        }
        summaryCrawler.getSummaryInfo(website, function(err){
           if (!err){
               _this.saveToRequest(request._id, website._id, 'info', callback);
           }
        });
    });
};

//TODO not ready
_this.makeLinkedInTest = function(request, callback) {
    var parsedUrl = urlUtil.parse(request.url, true);
    var username = parsedUrl.pathname.substring(1);
    linkedinCrawler.getInfo(username, function(err, response) {
        if (!err) {
            console.info('Finished looking up LinkedIn account');
            _this.saveToRequest(request._id, response._id, 'linkedin', callback);
        }
    });
};

_this.makeTwitterTest = function(request, callback) {
    var parsedUrl = urlUtil.parse(request.url, true);
    var username = parsedUrl.pathname.substring(1);
    twitterCrawler.getInfo(username, function(err, response) {
        if (!err) {
            console.info('Finished looking up Twitter account');
            _this.saveToRequest(request._id, response._id, 'twitter', callback);
        }
    });
};


_this.makeFacebookTest = function(request, callback) {
    var parsedUrl = urlUtil.parse(request.url, true);
    var username = parsedUrl.pathname.substring(1);
    facebookCrawler.getInfo(username, function(err, response) {
        if (!err) {
            console.info('Finished looking up Facebook account');
            _this.saveToRequest(request._id, response._id, 'facebook', callback);
        }
    });
};


_this.saveToRequest = function(requestId, testId, testName, callback){
    ApiRequest.findById(requestId, function(err, request){
        if( err ) {
            return console.error(err);
        }
        if( !request ) {
            return console.error('Cant find request');
        }

        var newTest = {
            status: 'done',
            id: testId,
            type: testName
        };

        request.tests.push(newTest);
        request.save(function(err){
            if (!err){
                callback(null);
            }
        })
    });
};


_this.sendAnswer = function (requestId, err){

    var body = {};
    body.requestId = requestId;

    if (err){
        body.message = err;
    }else{
        body.message = 'done';
    }

    var options = {
        url: config.api.host + ":" + config.api.port + config.api.route,
        method: 'POST',
        body: body,
        json: true
    };
    request(options, function(error, response, answer){
        if (error){
            console.log(error);
        }
    });
};

module.exports = _this;