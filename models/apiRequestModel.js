var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var apiRequestSchema = new Schema({
    username: 'String',
    url: 'String',
    status: 'String',
    tests: []
});

module.exports = mongoose.model('apiRequest', apiRequestSchema);
