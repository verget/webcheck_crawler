var express = require('express');
var app = express();
var mongoose = require('mongoose');
var bodyParser = require('body-parser');

var appConfig = require('./config.js');
//var appErrors = require('./modules/errors');
//
//var mainCrawler = require('./crawlers/mainCrawler');

var apiRequest = require('./modules/apiRequest');

var Website = require('./models/websiteModel');

mongoose.connect(appConfig.server.mongodb);
var db = mongoose.connection;

app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());


app.post('/apiRequest', function(req, res, next) {
    apiRequest.setRequest(req.body.requestId, function(err, response){
        if (!err){
            console.info('Request received');
            res.send(response);
        }else{
            console.log(err);
        }
    });

});

db.once('open', function() {
    app.listen(appConfig.server.port, function() {
        console.info('Crawler listening on port ' + appConfig.server.port);
    });
});

db.on('error', console.error.bind(console, 'connection error:'));